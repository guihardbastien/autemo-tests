from sgnlp.models.emotion_entailment import (
    RecconEmotionEntailmentConfig,
    RecconEmotionEntailmentModel,
    RecconEmotionEntailmentTokenizer,
    RecconEmotionEntailmentPreprocessor,
    RecconEmotionEntailmentPostprocessor,
)

config = RecconEmotionEntailmentConfig.from_pretrained(
    "https://storage.googleapis.com/sgnlp/models/reccon_emotion_entailment/config.json"
)
model = RecconEmotionEntailmentModel.from_pretrained(
    "https://storage.googleapis.com/sgnlp/models/reccon_emotion_entailment/pytorch_model.bin",
    config=config,
)
tokenizer = RecconEmotionEntailmentTokenizer.from_pretrained("roberta-base")
preprocessor = RecconEmotionEntailmentPreprocessor(tokenizer)
postprocess = RecconEmotionEntailmentPostprocessor()

input_batch = {
  "emotion": [
    "happiness",
    "neutral",
    "anger",
    "fear",
    "sadness"
  ],
  "target_utterance": [
    "It bothered me a lot",
    "It bothered me a lot",
    "It bothered me a lot",
    "It bothered me a lot",
    "It bothered me a lot",
  ],
  "evidence_utterance": [
    "an old lady didn't say hello to the baker",
    "an old lady didn't say hello to the baker",
    "an old lady didn't say hello to the baker",
    "an old lady didn't say hello to the baker",
    "an old lady didn't say hello to the baker"
  ],
  "conversation_history": [
    "Last time I went to the bakery, an old lady didn't say hello to the baker. It bothered me a lot",
    "Last time I went to the bakery, an old lady didn't say hello to the baker. It bothered me a lot",
    "Last time I went to the bakery, an old lady didn't say hello to the baker. It bothered me a lot",
    "Last time I went to the bakery, an old lady didn't say hello to the baker. It bothered me a lot",
    "Last time I went to the bakery, an old lady didn't say hello to the baker. It bothered me a lot",

  ]
}

input_dict = preprocessor(input_batch)
raw_output = model(**input_dict)
output = postprocess(raw_output)
print(output)
