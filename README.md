# AUTEMO

## Description

TBA

## Overview

TBA

### Directory structure

```bash
.
├── autemo.py
├── CHANGELOG.md
├── datasets
│   ├── test.csv
│   └── train.csv
├── embeddings
│   └── english
│       ├── glove.6B.100d.txt
│       ├── glove.6B.200d.txt
│       ├── glove.6B.300d.txt
│       └── glove.6B.50d.txt
├── emotion_detection.py
├── README.md
└── tests
```

### Features

TBA

### How to contribute to this library?

TBA

### How to use ?

Download glove word embeddings

```bash
cd embeddings/english
wget http://nlp.stanford.edu/data/glove.6B.zip
```

## Other/Optional considerations

TBA

## Ressources

- https://docs.python.org/3/library/argparse.html#module-argparse
- https://colab.research.google.com/github/karankishinani/Emotion-detection-from-text-using-PyTorch-and-Federated-Learning/blob/master/Emotion_detection_from_text_using_PyTorch.ipynb#scrollTo=rufFNufqDmUO
